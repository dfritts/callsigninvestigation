switch "All"
    case "All"
        ACID = ACID_all;
        ICAO = ICAO_all;
    case "Airborne"
        ACID = ACID_all(Gbs == 0);
        ICAO = ICAO_all(Gbs == 0);
    case "GT1000"
        ACID = ACID_all(FlightLevel > 10);
        ICAO = ICAO_all(FlightLevel > 10);     
    case "GT10000"
        ACID = ACID_all(FlightLevel > 100);
        ICAO = ICAO_all(FlightLevel > 100);    
end

reportsContainQ = 0;
AcidQ = [];
for i=1:length(ICAO)
    if (contains(ACID(i), "?"))
        reportsContainQ=reportsContainQ+1;
        AcidQ = [AcidQ; ACID(i)];
    end
end
AcidQ = unique(AcidQ);

totalICAO = length(ICAO);
reportsEmpty = sum(ACID == "");
reportsBlank = sum(ACID == "        ");
uICAO = unique(ICAO);
uniqueICAO = length(uICAO);
uniqueACID = length(unique(ACID));
acidpericao = zeros(size(uICAO));
routineTransition = 0;
singleBlankTransition = 0;
alwaysBlank = 0;
alwaysGood = 0;
alwaysBad = 0;
coastedOut = 0;
garbled = 0;
coastedOutSmall = 0;
for i=1:length(uICAO) 
    curICAO = uICAO(i);
    curACIDs = ACID(ICAO == curICAO);
    acidpericao(i) = length(unique(curACIDs));
    if (acidpericao(i) == 1)
        uCurACIDS = unique(curACIDs);
        if (uCurACIDS(1) == "")
            alwaysBlank = alwaysBlank +1;
        else
            if (contains(uCurACIDS(1), "?"))
                alwaysBad = alwaysBad +1;
            else
                alwaysGood = alwaysGood + 1;
            end
        end
    end
    if (acidpericao(i) == 2) 
        uCurACIDS = unique(curACIDs);
        if (uCurACIDS(1) ~= "" && uCurACIDS(2) ~= "" && ~contains(uCurACIDS(1), "?") && ~contains(uCurACIDS(2), "?"))
            routineTransition = routineTransition +1;
            % figure
            % plot(curACIDs == uCurACIDS(1))
        end
        if (contains(uCurACIDS(1), "?") || contains(uCurACIDS(2), "?"))
            garbled = garbled +1;
        end
        if (uCurACIDS(1) == "" || uCurACIDS(2) == "")
            transitions = 0;
            for j=1:length(curACIDs)-1
                if (curACIDs(j) ~= curACIDs(j+1))
                    transitions = transitions +1;
                end
            end
            if (transitions == 1 && sum(curACIDs == "") < 3 && curACIDs(1) == "")
                singleBlankTransition = singleBlankTransition + 1;
            else
                coastedOut = coastedOut +1;
                if (sum(curACIDs == "") < 5)
                    coastedOutSmall = coastedOutSmall+1;
                end
            end
        end
    end
end
acidPerICAO1 = sum(acidpericao==1);
acidPerICAO2 = sum(acidpericao==2);
acidPerICAO3 = sum(acidpericao==3);
acidPerICAOMore = sum(acidpericao>3);
clear acidpericao uCurACIDS curACIDs curACID i j transitions uICAO curICAO
